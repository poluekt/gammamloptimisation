# Optimisation of model-independent CKM gamma measurement using machine learning

## Introduction

This project is a further extension of the method to perform the model-independent measurement of the CKM angle $`\gamma`$ in a statistically more optimal way than the binned procedure [1]. The alternative technique that was proposed is based on using the family of weight functions rather than bins. It was shown that using Fourier transformation of strong phase difference, better statistical accuracy can be achieved, although it is still somewhat worse than the unbinned approach [2,3]. 

In this project, the attempt is made to obtain the set of weight functions that would provide the best possible statistical sensitivity. The weight functions are represented using fully-connected ANNs, and the training procedure is set up that uses the figure of merit similar to the binned $`Q^2`$ factor from [1] to optimise the sensitivity. 

Details on the construction of the figure of merit, and the ANN training strategy, are given in [4]. 

## Prerequisites

To run the code, you need two TensorFlow-based packages, [AmpliTF](https://github.com/apoluekt/AmpliTF) and [TensorFlowAnalysis2](https://github.com/apoluekt/TFA2). Instructions how to install these packages, together with the TensorFlow environment using Conda, can be found [here](https://github.com/apoluekt/TFA2/blob/master/doc/01_installation.md). 

## Running the code

Once the installation is done, activate the Conda `tfa` environment and run the [shell script](https://gitlab.cern.ch/poluekt/gammamloptimisation/-/blob/master/runme.sh) provided: 
```bash
$ conda activate tfa
$ . runme.sh
```
*Warning*: This script takes a few hours to run even on a powerful GPU machine. 

## Documentation

   1. Binned model-independent approach: https://arxiv.org/abs/0801.0840
   1. Unbinned Fourier-based approach: https://arxiv.org/abs/1712.08326
   1. Fourier-based code repository: https://gitlab.cern.ch/poluekt/FourierGamma
   1. Note in Overleaf with the detailed description of optimisation procedure: https://www.overleaf.com/read/vhmttjskhwtk
