import tensorflow as tf
import numpy as np
import argparse

import matplotlib
import matplotlib.pyplot as plt
from itertools import combinations

# Limit GPU memory usage to 8Gb
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus : 
    tf.config.experimental.set_virtual_device_configuration(gpus[0], 
         [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=1024*8)] )

import amplitf.interface as atfi
import amplitf.dynamics as atfd
import amplitf.kinematics as atfk
import amplitf.likelihood as atfl
import tfa.plotting as tfp
import tfa.neural_nets as tfn
import tfa.rootio as tfr
from amplitf.phasespace.dalitz_phasespace import DalitzPhaseSpace

import sys, math, os

# Masses of the initial and final state particles 
md  = 1.869
mpi = 0.139
mk  = 0.497

# Blatt-Weisskopf radial constants
dd = atfi.const(5.)
dr = atfi.const(1.5)

# Auxiliary function to create the resonance of mass "m", width "w", spin "spin" in the 
# channel "ch", as a function of Dalitz plot variables vector "x"
def resonance(x, m, w, spin, ch, dlz) :
    if ch == 0 : 
      return atfd.breit_wigner_lineshape(dlz.m2ab(x), m,  w, mpi, mk, mpi, md, dr, dd, spin, spin, barrier_factor = False)*\
             atfk.zemach_tensor(dlz.m2ab(x), dlz.m2ac(x), dlz.m2bc(x), atfi.const(md**2), atfi.const(mpi**2), atfi.const(mk**2), atfi.const(mpi**2), spin)
    if ch == 1 : 
      return atfd.breit_wigner_lineshape(dlz.m2bc(x), m,  w, mk, mpi, mpi, md, dr, dd, spin, spin, barrier_factor = False)*\
             atfk.zemach_tensor(dlz.m2bc(x), dlz.m2ab(x), dlz.m2ac(x), atfi.const(md**2), atfi.const(mk**2), atfi.const(mpi**2), atfi.const(mpi**2), spin)
    if ch == 2 : 
      return atfd.breit_wigner_lineshape(dlz.m2ac(x), m,  w, mpi, mpi, mk, md, dr, dd, spin, spin, barrier_factor = False)*\
             atfk.zemach_tensor(dlz.m2ac(x), dlz.m2bc(x), dlz.m2ab(x), atfi.const(md**2), atfi.const(mpi**2), atfi.const(mpi**2), atfi.const(mk**2), spin)
    if ch == 3 : 
      return atfd.gounaris_sakurai_lineshape(dlz.m2ac(x), m,  w, mpi)*\
             atfk.zemach_tensor(dlz.m2ac(x), dlz.m2bc(x), dlz.m2ab(x), atfi.const(md**2), atfi.const(mpi**2), atfi.const(mpi**2), atfi.const(mk**2), spin)
    if ch == 4 : 
      return atfd.flatte_lineshape(dlz.m2ac(x), m, 0.09, 0.02, mpi, mpi, mk, mk)*\
             atfk.zemach_tensor(dlz.m2ac(x), dlz.m2bc(x), dlz.m2ab(x), atfi.const(md**2), atfi.const(mpi**2), atfi.const(mpi**2), atfi.const(mk**2), spin)
    return None

class D2KsPiPi : 
  def __init__(self, reduced = False, inputfile = None) : 

    # Create Dalitz plot phase space 
    self.dlz = DalitzPhaseSpace(mpi, mk, mpi, md)

    params={"a0r" : ( -0.756841, 0.180804), 
            "a0i" : ( -0.719579, 0.187009),
            "a1r" : (  1.097505, 0.034081),
            "a1i" : (  0.419228, 0.033228),
            "a2r" : ( -0.196752, 0.036479),
            "a2i" : (  0.016840, 0.043689),
            "a3r" : (  0.020875, 0.001096),
            "a3i" : (  0.018290, 0.001195),
            "a4r" : (  0.051761, 0.002613),
            "a4i" : (  0.041137, 0.001897),
            "a5r" : (  0.093466, 0.010187),
            "a5i" : (  0.255268, 0.010483),
            "a6r" : ( -1.357615, 0.069999),
            "a6i" : (  0.040836, 0.059655),
            "a7r" : (  2.988896, 0.127042),
            "a7i" : ( -1.878055, 0.120555),
            "a8r" : (  1.327757, 0.019316),
            "a8i" : ( -1.145395, 0.017091),
            "a9r" : (  0.745753, 0.048381),
            "a9i" : ( -0.744076, 0.052081),
            "a10r" : ( -2.415071, 0.043866),
            "a10i" : ( -1.012651, 0.050998),
            "a11r" : ( -0.686463, 0.030458),
            "a11i" : (  0.131047, 0.030777),
            "a12r" : (  0.042872, 0.300616),
            "a12i" : ( -1.301729, 0.312203),
            "a13r" : ( -0.123027, 0.004991),
            "a13i" : (  0.041238, 0.005045),
            "a14r" : (  0.151902, 0.052192),
            "a14i" : (  0.962249, 0.056412),
            "a15r" : ( -0.528121, 0.037820),
            "a15i" : ( -0.316514, 0.045555),
            "a16r" : ( -0.285350, 0.034573),
            "a16i" : (  0.392055, 0.028871),
            "a17r" : (  0.345416, 0.293911),
            "a17i" : (  3.900965, 0.267996), }

    self.coeffs = []
    for n in range(18) : 
        pname = "a%d" % n
        pr = atfi.const(params[pname + "r"][0])
        pi = atfi.const(params[pname + "i"][0])
        self.coeffs += [ atfi.complex(pr, pi) ]

  def amplitude(self, x) : 
    ampl  = atfi.complex(atfi.const(0.), atfi.const(0.))

    ampl += self.coeffs[0]

    ampl += -atfi.complex(atfi.const(1.), atfi.const(0.))*resonance(x, atfi.const(0.775500), atfi.const(0.149400), 1, 3, self.dlz)
    ampl +=  self.coeffs[1]*resonance(x, atfi.const(0.522477), atfi.const(0.453106), 0, 2, self.dlz)
    ampl += -self.coeffs[2]*resonance(x, atfi.const(1.0),   atfi.const(0.4550),      1, 2, self.dlz)
    ampl += -self.coeffs[3]*resonance(x, atfi.const(0.782650), atfi.const(0.008490), 1, 2, self.dlz)
    ampl +=  self.coeffs[4]*resonance(x, atfi.const(0.97700),  atfi.const(0.1),      0, 4, self.dlz)
    ampl +=  self.coeffs[5]*resonance(x, atfi.const(1.033172), atfi.const(0.087984), 0, 2, self.dlz)
    ampl +=  self.coeffs[6]*resonance(x, atfi.const(1.275400), atfi.const(0.185200), 2, 2, self.dlz)
    ampl +=  self.coeffs[7]*resonance(x, atfi.const(1.434000), atfi.const(0.173000), 0, 2, self.dlz)

    ampl += -self.coeffs[8]*resonance(x, atfi.const(0.891660), atfi.const(0.050800), 1, 0, self.dlz)
    ampl += -self.coeffs[9]*resonance(x, atfi.const(1.414000), atfi.const(0.232000), 1, 0, self.dlz)
    ampl +=  self.coeffs[10]*resonance(x, atfi.const(1.414000), atfi.const(0.290000), 0, 0, self.dlz)
    ampl +=  self.coeffs[11]*resonance(x, atfi.const(1.425600), atfi.const(0.098500), 2, 0, self.dlz)
    ampl += -self.coeffs[12]*resonance(x, atfi.const(1.717000), atfi.const(0.322000), 1, 0, self.dlz)

    ampl += self.coeffs[13]*resonance(x, atfi.const(0.891660), atfi.const(0.050800), 1, 1, self.dlz)
    ampl += self.coeffs[14]*resonance(x, atfi.const(1.414000), atfi.const(0.232000), 1, 1, self.dlz)
    ampl += self.coeffs[15]*resonance(x, atfi.const(1.414000), atfi.const(0.290000), 0, 1, self.dlz)
    ampl += self.coeffs[16]*resonance(x, atfi.const(1.425600), atfi.const(0.098500), 2, 1, self.dlz)
    ampl += self.coeffs[17]*resonance(x, atfi.const(1.717000), atfi.const(0.322000), 1, 1, self.dlz)

    return ampl

  def density(self, x) :
    return atfd.density( self.amplitude(x) )

  def phase(self, x) : 
    a = self.amplitude(x)
    return atfi.atan2(atfi.imaginary(a), atfi.real(a))


def ann_weights(pd, pdbar, c, s, w, b) :
  """
     Calculate weight functions parametrised as fully-connected ANNs
     Inputs: 
       pd    : vector of pD values (array-like structure, e.g. 1D TF tensor of shape (N,))
       pdbar : vector of bar(pD) values
       c     : vector of cos(strong phase difference)
       s     : vector of sin(strong phase difference)
       w     : ANN neuron weights
       b     : ANN neuron biases
     Output: 
       2D TF tensor of shape (N, M), where the 1st dimension if the point over phase space, 
             and 2nd dimension is the weight function number (0... M-1)
       The output function values are normalised to lie in the range (-1., 1.). 
  """
  sqrtp = atfi.sqrt(pd)
  sqrtpb = atfi.sqrt(pdbar)
  sqrtpp = sqrtp*sqrtpb
  z = atfi.stack([sqrtp, sqrtpb, c/sqrtpp, s/sqrtpp], axis = 1)
  return tfn.multilayer_perceptron(z, ((0., 5.), (0., 5.), (-1., 1.), (-1., 1.)), w, b, multiple = True)*2.-1.

def bins(x, c, s, nbins) : 
  """
     Calculate the tensor representing bins over the phase space, following the 
     strong phase difference binning. It is of the same form as for the 
     above ANN weight parametrisation. 
     Inputs: 
       x   : 2D tensor of shape (N, 2) with Dalitz plot variables
       c   : vector of cos(strong phase difference)
       s   : vector of sin(strong phase difference)
       nbins : number of bins
     Output: 
       2D tensor of shape (N, 2*nbins) of binary (0/1) values, where 1 means that the input Dalitz plot 
          point belong to a particular bin. 
  """
  phase = atfi.atan2(s, c)
  nbin = (phase/atfi.pi()+1)/2.*nbins
  w = [ atfi.logical_and(x[:,0]<x[:,1], atfi.logical_and(nbin>=i, nbin<i+1)) for i in range(nbins) ] + [
        atfi.logical_and(x[:,0]>x[:,1], atfi.logical_and(nbin>=i, nbin<i+1)) for i in range(nbins) ]
  return atfi.stack(tf.dtypes.cast(w, tf.float64), axis = 1)

@atfi.function
def q2factor(nwghts, pd, c, s) :
  """
    Function to calculate the figure of merit Q^2 of the family of weight functions. 
    Inputs: 
      nwght : list of normalised vectors of weights (normalised such that integral(w**2 * pd)=1, see function below)
      pd    : vector of pD values (pD does not need to be normalised)
      c     : vector of cos(strong phase difference)
      s     : vector of sin(strong phase difference)
    Output: 
      Q^2 value
  """
  q2 = 0.
  #pdint2 = atfl.integral(pd)**2   # pD integral squared
  for nw in nwghts :
    cw = atfl.integral(c*nw)
    sw = atfl.integral(s*nw)
    q2 += (cw**2 + sw**2)
  return q2

def norm_weights(wght, pd) : 
  """
    Normalise the weight functions
    Inputs: 
      wght : 2D tensor of unnormalised weights (e.g. output of ann_weights)
      pd   : vector of pD values (pD does not need to be normalised)
    Output: 
      List of normalised weight functions (integral(w**2*pd)=1)
  """
  nwght = []
  for i in range(wght.shape[1]) : 
    nwght += [ wght[:,i] / atfi.sqrt(atfl.integral(pd*wght[:,i]**2)) ]
  return nwght

def covariance(nwght, pd) : 
  """
    Calculate the dictionary representing the covariance matrix
    Inputs: 
      nwght : list of vectors of weights
      pd : vector of pD values
    Output: 
      dictionary of covariances with keys as (n,m)
  """
  corrs = {}
  for i,j in combinations(range(len(nwght)), 2) : 
    if i != j : 
      corr = atfl.integral(pd*nwght[i]*nwght[j])
      corrs[(i,j)] = corr
  return corrs

def main() :

  parser = argparse.ArgumentParser(description = "ANN weights optimisation for gamma measurement", 
                                   formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument('--m', type=int, default = 3, 
                      help="Number of functions to train (3-5)")
  parser.add_argument('--seed', type=int, default = 1, 
                      help="Initial random seed")
  parser.add_argument('--pixels', type=int, default = 500, 
                      help="Number of Dalitz plot pixels")
  parser.add_argument('--output', type=str, default = "output", 
                      help="Output file prefix")
  parser.add_argument('--input', type=str, default = None, 
                      help="Input file with ANN parameters (.npy format)")
  parser.add_argument('--reg', type=float, default = 1e-3, 
                      help="L2 regularisation parameter")
  parser.add_argument('--ort', type=float, default = 10., 
                      help="Orthogonality penalty")
  parser.add_argument('--lr', type=float, default = 5e-4, 
                      help="Learning rate")
  parser.add_argument('--epochs', type=int, default = 500000, 
                      help="Number of optimisation epochs")
  parser.add_argument('--draw', type=int, default = 100, 
                      help="Number of epochs between plotting")

  #parser.add_argument('--gamma', default = False, action = "store_const", const = True, 
  #                      help='Generate D*0 -> D0gamma mode (default is D*0 -> D0pi0)')

  args = parser.parse_args()

  if len(sys.argv)<1 : 
    parser.print_help()
    raise SystemExit

  l2 = args.reg
  ort_penalty = args.ort
  learning_rate = args.lr
  epochs = args.epochs
  npixels = args.pixels
  seed = args.seed
  M = args.m  # Number of weight functions to train

  dkpp = D2KsPiPi()  # D->Kspipi amplitude object
  dlz = dkpp.dlz     # Dalitz plot phase space object
  bounds = [(dlz.minab, dlz.maxab), (dlz.minbc, dlz.maxbc)]  # Boundaries of Dalitz plot

  atfi.set_seed(seed)  # Random seed
  np.random.seed(seed)

  x = dlz.rectangular_grid_sample(npixels, npixels).numpy()  # 2D tensor for the grid of Dalitz plot points
  xbar = x[:,(1,0)]                       # The same points reflected wrt. diagonal axis
  ampl = dkpp.amplitude(x).numpy()        # Complex amplitude values in these points
  amplbar = dkpp.amplitude(xbar).numpy()  # Conjugated amplitude values

  pd = atfd.density(ampl)                 # pD values
  pdbar = atfd.density(amplbar)           # bar(pD) values

  pdint = atfl.integral(pd)
  pd /= pdint                 # pD and bar(pD) should be normalised to unit integral
  pdbar /= pdint

  ph = atfi.atan2(atfi.imaginary(ampl), atfi.real(ampl))            # Strong phase
  phbar = atfi.atan2(atfi.imaginary(amplbar), atfi.real(amplbar))   # Conjugated strong phase

  dphi = ph - phbar                       # Strong phase difference between symmetric points
  c = atfi.sqrt(pd*pdbar)*atfi.cos(dphi)  # cos(strong phase difference)
  s = atfi.sqrt(pd*pdbar)*atfi.sin(dphi)  # sin(strong phase difference)

  ############################################################################
  ### This part just calculates the Q values for some particular weight sets #
  ############################################################################

  print("Q values for various sets of weight functions: ")

  ########## Strong phase binning with 8 bins 
  nbins = 8
  nwght = norm_weights(bins(x, c, s, nbins), pd)
  q2 = q2factor(nwght, pd, c, s)
  print(f"  Q({nbins}bins)={math.sqrt(q2)}")

  ########## Strong phase binning with 20 bins 
  nbins = 20
  nwght = norm_weights(bins(x, c, s, nbins), pd)
  q2 = q2factor(nwght, pd, c, s)
  print(f"  Q({nbins}bins)={math.sqrt(q2)}")

  ########## Fourier weights with M=1, not split 
  wght = np.stack([ atfi.ones(pd), atfi.cos(dphi), atfi.sin(dphi) ], axis = 1)
  nwght = norm_weights(wght, pd)
  q2 = q2factor(nwght, pd, c, s)
  print(f"  Q(1,cos,sin)={math.sqrt(q2)}")

  ########## Fourier weights with M=1, split separately for pD<bar(pD) and pd>bar(pD)
  split1 = tf.dtypes.cast(pd>pdbar, tf.float64)
  split2 = tf.dtypes.cast(pd<pdbar, tf.float64)
  wght = np.stack([ 
                    atfi.ones(pd)*split1, atfi.cos(dphi)*split1, atfi.sin(dphi)*split1, 
                    atfi.ones(pd)*split2, atfi.cos(dphi)*split2, atfi.sin(dphi)*split2 
                  ], axis = 1)
  nwght = norm_weights(wght, pd)
  q2 = q2factor(nwght, pd, c, s)
  print(f"  Q(1,cos,sin,split)={math.sqrt(q2)}")

  ########## Fourier weights divided by pD
  wght = np.stack([ atfi.ones(pd)/pd, atfi.cos(dphi)/pd, atfi.sin(dphi)/pd ], axis = 1)
  nwght = norm_weights(wght, pd)
  q2 = q2factor(nwght, pd, c, s)
  print(f"  Q(1/pd,cos/pd,sin/pd)={math.sqrt(q2)}")

  ########## Fourier weights divided by sqrt(pD)
  wght = np.stack([ atfi.ones(pd)/atfi.sqrt(pd), atfi.cos(dphi)/atfi.sqrt(pd), atfi.sin(dphi)/atfi.sqrt(pd) ], axis = 1)
  nwght = norm_weights(wght, pd)
  cov = covariance(nwght, pd)
  q2 = q2factor(nwght, pd, c, s)
  print(f"  Q(1/sqrt(pd),cos/sqrt(pd),sin/sqrt(pd))={math.sqrt(q2)}")
  print(f"     Cov = {cov}")

  print("Other auxiliary information: ")

  cint = atfl.integral(c)/atfl.integral(pd)
  sint = atfl.integral(s)/atfl.integral(pd)
  print(f"  integral(C)={cint}, integral(S)={sint}")

  cosint = atfl.integral(atfi.cos(dphi))
  sinint = atfl.integral(atfi.sin(dphi))
  print(f"  integral(cos)={cosint}, integral(sin)={sinint}")

  print("Initialising ANN")
  # initialise ANN parameters
  if args.input :   # Read from file if input is given
    init = np.load(args.input, allow_pickle = True)
    w, b = tfn.init_weights_biases(init)
  else : 
    # Otherwise initialise random ANN parameters
    w, b = tfn.create_weights_biases(4, [20, 10], n_output = M)

  @atfi.function
  def loss_components() : 
    """
      Calculate the components of loss function for ANN training
      Takes the precalculated pD, bar(pD) etc. as input, as well as the 
      ANN parameters w, b

      Output: (q2, reg, ort, nwght, cov), where
        q2: Q^2 value for a given set of weight functions
        reg : L2 regularisation term (propto sum of squares of ANN weights w)
        ort : Orthogonality penalty term (propto sum of squares of off-diagonal elements of covariance matrix)
        nwght : list of vectors of normalised weights
        cov : covariance dictionary
    """
    wght = ann_weights(pd, pdbar, c, s, w, b)  # Calculating weight functions
    reg = l2*tfn.l2_regularisation(w)      # L2 regularisation term
    ort = 0.
    nwght = norm_weights(wght, pd)         # Normalised weights
    q2 = q2factor(nwght, pd, c, s)  # Q^2 value
    cov = {}
    # Calculate covariance and orthogonality penalty term
    for i,j in combinations(range(wght.shape[1]), 2) : 
      if i != j : 
        _cov = atfl.integral(pd*nwght[i]*nwght[j])
        cov[(i,j)] = _cov
        ort += ort_penalty*_cov**2
    return q2, reg, ort, nwght, cov

  @atfi.function
  def loss() : 
    """
      Calculate loss function to be minimised
    """
    q2, reg, ort, nwght, cov = loss_components()
    return -q2 + reg + ort

  q2, reg, ort, nwght, cov = loss_components()
  print(f"Init covariances={cov}")

  print(f"Weighted integrals: ")
  for i,nw in enumerate(nwght) : 
    pdi = atfl.integral(pd*nw)
    pdbari = atfl.integral(pdbar*nw)
    ci = atfl.integral(c*nw)
    si = atfl.integral(s*nw)
    print(f"  {i} : pDi={pdi}, bar(pD)i={pdbari}, Ci={ci}, Si={si}")

  # Open graphic window
  tfp.set_lhcb_style(size = 12, usetex = True)
  ncols = (M+1)//2
  fig, ax = plt.subplots(nrows = 2, ncols = ncols, figsize = (3.5*ncols, 6) )
  labels = (r"$M^2(K_S^0\pi^+)$", r"$M^2(K_S^0\pi^-)$")
  tfp.plot_distr2d(x[:,0], x[:,1], (npixels-1, npixels-1), bounds + [(np.amax(pd)/1e3, np.amax(pd))], 
                   fig, ax[0,0], labels = labels, weights = pd, log = True,
                   ztitle = r"$p_{D}(z)$", title = r"$p_{D}(z)$")
  for n in range(M) : 
    tfp.plot_distr2d(x[:,0], x[:,1], (npixels-1, npixels-1), bounds + [(-2.0, 2.0)], fig, ax[(n+1)//ncols, (n+1)%ncols], labels = labels, 
                       weights = (nwght[n]), colorbar = True, cmap = "seismic", title = f"$w_{{{n}}}(z)$", ztitle = f"$w_{{{n}}}(z)$")
  plt.tight_layout(pad=1., w_pad=1., h_pad=1.)
  plt.draw()
  plt.pause(0.1)

  # Starting optimisation
  print("Starting ANN training")

  opt = tf.keras.optimizers.Adam(learning_rate=learning_rate)
  lbest = 1e10  # intialise the best loss value so far

  for i in range(epochs) : 
    if i % args.draw == 0 : # Each args.draw epochs check the status of optimisation
      l = loss().numpy()                            # Current loss
      q2, reg, ort, nwght, cov = loss_components()  # And its components
      print(f"  Epoch {i}, loss={l}, q2={q2}, ort={ort}, reg={reg}, lbest={lbest}")
      if l > lbest : continue

      # If found better loss, draw this solution and store to files
      lbest = l
      for n in range(M) : 
        ax[(n+1)//ncols, (n+1)%ncols].clear()
        tfp.plot_distr2d(x[:,0], x[:,1], (npixels-1, npixels-1), bounds + [(-2.0, 2.0)], fig, ax[(n+1)//ncols, (n+1)%ncols], 
                       labels = labels, weights = (nwght[n]), colorbar = False, cmap = "seismic", title = f"$w_{{{n}}}(z)$", 
                       ztitle = f"$w_{{{n}}}(z)$")
      plt.draw()
      plt.pause(0.1)

      fig.savefig(f"{args.output}.pdf")
      np.save(f"{args.output}.npy", [w, b] )
      np.savez(f"{args.output}.npz", x=x, nwght=nwght)
      tfr.write_tuple(f"{args.output}.root", np.stack([x[:,0], x[:,1], pd, pdbar, c, s] + nwght, axis=1), 
                      branches = ["x","y","pd","pdb","c","s"] + [f"w{i}" for i in range(M)])
    opt.minimize(loss, [w, b]) # Minimisation step

if __name__ == "__main__" : 
  main()
