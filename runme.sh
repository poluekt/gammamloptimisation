python ann_weights.py --m 3 --seed 10 --lr 0.0005 --epochs 100000 --reg 0.0002 --ort 10. --pixels 500 --output m3_1 --draw 500
python ann_weights.py --m 3 --input m3_1.npy --seed 20 --lr 0.0002 --epochs 200000 --reg 0.0002 --ort 100. --pixels 500 --output m3_2 --draw 500

python ann_weights.py --m 5 --seed 30 --lr 0.0005 --epochs 200000 --reg 0.0001 --ort 10. --pixels 500 --output m5_1 --draw 500
python ann_weights.py --m 5 --input m5_1.npy --seed 40 --lr 0.0002 --epochs 400000 --reg 0.0001 --ort 100. --pixels 500 --output m5_2 --draw 500
